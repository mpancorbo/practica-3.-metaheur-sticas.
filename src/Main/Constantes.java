package Main;

public class Constantes {

    private Constantes(){}

    public static final int MEMETICO = 1;

    public static final int GREEDY = 2;

    public static final int AM10_1 = 1;

    public static final int AM10_01 = 2;

    public static final int AM10_01Mejores = 3;

    public static final int CrucePosicion = 1;

    public static final int CrucePMX = 2;

    public static final int CruceOX = 3;

    public static final String[] PROBLEMAS = {"Els19", "Chr20a", "Chr25a", "Nug25", "Bur26a", "Bur26b", "Tai30a", "Tai30b", "Esc32a", "Kra32", "Tai35a", "Tai35b", "Tho40", "Tai40a", "Sko42", "Sko49", "Tai50a", "Tai50b", "Tai60a", "Lipa90a"};

}
