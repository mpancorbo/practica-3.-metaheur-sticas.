package Main;

import QAP.AlgoritmosMemeticos.*;
import QAP.Greedy;
import QAP.ModelSolucion;
import QAP.SolucionQAP;
import Util.Print;
import Util.PrintToConsole;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Practica3 {
    public static void main(String[] arg){


        Print printer = new PrintToConsole();

        printer.printCabecera();

        int tipoSolucion = printer.menuTipoSolucion();
        int tipoHibridacion = 0;
        int tipoCruce = 0;
        if(tipoSolucion == Constantes.MEMETICO){
            tipoHibridacion = printer.menuTipoHibridacion();
            tipoCruce = printer.menuTipoCruce()-1;
        }
        int problemIndex = printer.menuProblemas()-1;
        int numIteraciones = 1;
        if(problemIndex !=21 && tipoSolucion != Constantes.GREEDY) numIteraciones = printer.menuIteraciones();
        long semilla = 0;
        if(tipoSolucion != Constantes.GREEDY){
            semilla = printer.menuSemilla();
        }
        //715225739

        String path;
        AlgoritmoMemetico memetico;
        SolucionQAP greedy;


        try{
            switch (tipoSolucion) {
                case Constantes.MEMETICO:
                    if (problemIndex != Constantes.PROBLEMAS.length) {

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        memetico = AlgoritmoMemeticoFactory.getInstance(tipoHibridacion, tipoCruce, semilla, path);

                        ArrayList<ModelSolucion> solucionMemetico = memetico.getNsoluciones(numIteraciones);
                        printer.printResultados(solucionMemetico, problemIndex);

                    } else {
                        for (int i = 0; i < Constantes.PROBLEMAS.length; i++) {

                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            memetico = AlgoritmoMemeticoFactory.getInstance(tipoHibridacion, tipoCruce, semilla, path);

                            ArrayList<ModelSolucion> solucionMemetico = memetico.getNsoluciones(numIteraciones);
                            printer.printResultados(solucionMemetico, i);
                        }
                    }
                    break;
                case Constantes.GREEDY:
                    if (problemIndex != Constantes.PROBLEMAS.length) {

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        greedy = new Greedy(path);

                        ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);
                        printer.printResultados(solucionGreedy, problemIndex);

                    } else {
                        for (int i = 0; i < Constantes.PROBLEMAS.length; i++) {

                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            greedy = new Greedy(path);

                            ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);
                            printer.printResultados(solucionGreedy, i);
                        }
                    }
                    break;
            }
        }catch (FileNotFoundException e) {
            printer.mostrarFileNotFoundError(e);
            e.printStackTrace();
        }
    }
}
