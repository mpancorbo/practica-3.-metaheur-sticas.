package QAP;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

public abstract class SolucionQAP extends QAP {
    public SolucionQAP(String path, long semilla) throws FileNotFoundException {
        super(path, semilla);
    }

    public SolucionQAP(int tam, int[][] flujos, int[][] distancias, Random random) {
        super(tam, flujos, distancias, random);
    }

    public abstract ModelSolucion getSolucion();

    public ArrayList<ModelSolucion> getNsoluciones(int n){
        ArrayList<ModelSolucion> soluciones = new ArrayList<ModelSolucion>();

        for(int i = 0; i < n; i++)
            soluciones.add(getSolucion());

        return soluciones;
    }
}
