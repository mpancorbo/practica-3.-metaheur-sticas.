package QAP;

import Util.QAPReader;

import java.io.FileNotFoundException;
import java.util.Random;

public class QAP {

    protected int tamProblema;
    protected int flujos[][];
    protected int distancias[][];
    protected Random random;

    public QAP(String path, long semilla) throws FileNotFoundException{

        QAPReader reader = new QAPReader(path);

        this.tamProblema = reader.getTam();
        this.flujos = reader.getFlujos();
        this.distancias = reader.getDistancias();
        random = new Random(semilla);
    }
    public QAP(int tam, int[][] flujos, int[][] distancias, Random random){
        this.tamProblema = tam;
        this.flujos = flujos;
        this.distancias = distancias;
        this.random = random;
    }

    protected int funcionObjetivo(int[] solucion){
        int coste = 0;

        for(int i = 0; i < solucion.length; i++)
            for(int j = 0; j < solucion.length; j++)
                if(i != j)
                    coste += flujos[i][j]* distancias[solucion[i]][solucion[j]];

        return coste;
    }

    protected  int[] generaPermutacionAleatoria(int[] inicial){
        int limite = inicial.length-1;

        for(int i = limite; i > 0; i--) {
            int randomIndex = this.random.nextInt(i);
            int aux = inicial[i];
            inicial[i] = inicial[randomIndex];
            inicial[randomIndex] = aux;
        }

        return inicial;
    }

    protected int[] generaSolucionAleatoria(){
        int[] solucionAleatoria = new int[this.tamProblema];

        for(int i = 0; i < solucionAleatoria.length; i++){
            solucionAleatoria[i] = i;
        }

        return generaPermutacionAleatoria(solucionAleatoria);
    }

    protected int getDiffCoste(int[] solucion, int a, int b){

        int diff = 0;

        if (a == b)
            return 0;

        for(int i = 0; i < solucion.length; i++){
            if(i!=a && i!=b)
                diff += this.flujos[a][i]*(this.distancias[solucion[b]][solucion[i]] - this.distancias[solucion[a]][solucion[i]])
                        + this.flujos[b][i]*(this.distancias[solucion[a]][solucion[i]] - this.distancias[solucion[b]][solucion[i]])
                        + this.flujos[i][a]*(this.distancias[solucion[i]][solucion[b]] - this.distancias[solucion[i]][solucion[a]])
                        + this.flujos[i][b]*(this.distancias[solucion[i]][solucion[a]] - this.distancias[solucion[i]][solucion[b]]);
        }

        return diff;
    }
}
