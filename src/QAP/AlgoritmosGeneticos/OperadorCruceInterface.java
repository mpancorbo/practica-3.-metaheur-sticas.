package QAP.AlgoritmosGeneticos;

import Util.SortedArrayList;

public interface OperadorCruceInterface {

    public SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion, int numeroCruces);

}
