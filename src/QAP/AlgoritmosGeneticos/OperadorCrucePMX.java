package QAP.AlgoritmosGeneticos;

import QAP.QAP;
import Util.SortedArrayList;
import java.util.Random;

public class OperadorCrucePMX extends QAP implements OperadorCruceInterface {

    public OperadorCrucePMX(int tam, int[][] flujos, int[][] distancias, Random random) {
        super(tam, flujos, distancias, random);
    }

    @Override
    public SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion, int numeroCruces) {
        SortedArrayList<Cromosoma> hijos;

        for(int i = 0; i < numeroCruces; i++){
            int indexPadreA = this.random.nextInt(poblacion.size());
            int indexPadreB = this.random.nextInt(poblacion.size());

            hijos = this.operadorCrucePMX(poblacion.get(indexPadreA), poblacion.get(indexPadreB));

            poblacion.set(indexPadreA, hijos.get(0));
            poblacion.set(indexPadreB, hijos.get(1));
        }

        return poblacion;
    }

    protected SortedArrayList<Cromosoma> operadorCrucePMX(Cromosoma padre1, Cromosoma padre2){
        SortedArrayList<Cromosoma> hijos = new SortedArrayList<Cromosoma>();

        int puntoCorteA = this.random.nextInt(this.tamProblema);
        int puntoCorteB;

        do{
            puntoCorteB = this.random.nextInt(this.tamProblema);
        }while(puntoCorteA == puntoCorteB);

        if(puntoCorteA > puntoCorteB){
            int swapAux = puntoCorteA;
            puntoCorteA = puntoCorteB;
            puntoCorteB = swapAux;
        }

        hijos.add(getHjoPMX(padre1, padre2, puntoCorteA, puntoCorteB));
        hijos.add(getHjoPMX(padre2, padre1, puntoCorteA, puntoCorteB));

        return hijos;
    }

    private Cromosoma getHjoPMX(Cromosoma padre1, Cromosoma padre2, int puntoCorteA, int puntoCorteB){

        int[] hijo = new int[this.tamProblema];
        int[] permutacionPadre1 = padre1.getPermutacion();
        int[] permutacionPadre2 = padre2.getPermutacion();

        System.arraycopy(permutacionPadre1, puntoCorteA, hijo, puntoCorteA, puntoCorteB - puntoCorteA + 1);

        for(int i = 0; i < puntoCorteA; i++) {
            int index = getIndexOfValue(hijo, permutacionPadre2[i], puntoCorteA, puntoCorteB);
            int oldIndex = i;

            while(index != -1){
                oldIndex = index;
                index = getIndexOfValue(hijo, permutacionPadre2[index], puntoCorteA, puntoCorteB);
            }
            hijo[i] = permutacionPadre2[oldIndex];
        }

        for(int i = puntoCorteB + 1; i < this.tamProblema; i++) {
            int index = getIndexOfValue(hijo, permutacionPadre2[i], puntoCorteA, puntoCorteB);
            int oldIndex = i;

            while(index != -1){
                oldIndex = index;
                index = getIndexOfValue(hijo, permutacionPadre2[index], puntoCorteA, puntoCorteB);
            }
            hijo[i] = permutacionPadre2[oldIndex];
        }

        return new Cromosoma(hijo, funcionObjetivo(hijo));
    }

    private int getIndexOfValue(int[] array, int value, int puntoCorteA, int puntoCorteB){
        for(int i = puntoCorteA; i <= puntoCorteB; i++)
            if(array[i] == value)
                return i;

        return -1;
    }
}
