package QAP.AlgoritmosGeneticos;

import Util.SortedArrayList;

import java.io.FileNotFoundException;
import java.util.Random;

public class AGGeneracional extends AlgoritmoGenetico {

    protected static final double PROBABILIDAD_CRUCE = 0.7;
    protected static final int NUMERO_CRUCES = (int) ((AlgoritmoGenetico.TAM_POBLACION/2) * AGGeneracional.PROBABILIDAD_CRUCE);

    private OperadorCruceInterface operadorCruce;

    public AGGeneracional(String path, long semilla, int tipoOperadorCruce) throws FileNotFoundException {
        super(path, semilla);
        operadorCruce = getOperadorCruce(tipoOperadorCruce);
    }

    public AGGeneracional(int tam, int[][] flujos, int[][] distancias, Random random, int tipoOperadorCruce) {
        super(tam, flujos, distancias, random);
        operadorCruce = getOperadorCruce(tipoOperadorCruce);
    }

    @Override
    protected SortedArrayList<Cromosoma> operadorSeleccion(SortedArrayList<Cromosoma> poblacionInicial) {
        SortedArrayList<Cromosoma> padresSeleccionados = new SortedArrayList<Cromosoma>();

        for(int i = 0; i < AlgoritmoGenetico.TAM_POBLACION; i++)
            padresSeleccionados.add(torneoBinario(poblacionInicial));

        return padresSeleccionados;
    }

    @Override
    protected  SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion){
        this.usosFuncionObjetivo += AGGeneracional.NUMERO_CRUCES*2;

        return operadorCruce.operadorCruce(poblacion, AGGeneracional.NUMERO_CRUCES);
    }

    @Override
    protected SortedArrayList<Cromosoma> reemplazamiento(SortedArrayList<Cromosoma> poblacionInicial, SortedArrayList<Cromosoma> poblacionMutada) {
        Cromosoma mejorInicial = poblacionInicial.get(0);
        int indexPeorMutado = poblacionMutada.size() - 1;

        for(Cromosoma nuevo : poblacionMutada){
            if(nuevo.compareTo(mejorInicial) == 0)
                return poblacionMutada;
        }

        poblacionMutada.set(indexPeorMutado, mejorInicial);

        return poblacionMutada;
    }
}
