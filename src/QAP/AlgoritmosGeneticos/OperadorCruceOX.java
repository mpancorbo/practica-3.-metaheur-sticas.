package QAP.AlgoritmosGeneticos;

import QAP.QAP;
import Util.SortedArrayList;
import java.util.Random;

public class OperadorCruceOX extends QAP implements OperadorCruceInterface {

    public OperadorCruceOX(int tam, int[][] flujos, int[][] distancias, Random random) {
        super(tam, flujos, distancias, random);
    }

    @Override
    public SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion, int numeroCruces) {
        SortedArrayList<Cromosoma> hijos;

        for(int i = 0; i < numeroCruces; i++){
            int indexPadreA = this.random.nextInt(poblacion.size());
            int indexPadreB = this.random.nextInt(poblacion.size());

            hijos = this.operadorCruceOX(poblacion.get(indexPadreA), poblacion.get(indexPadreB));

            poblacion.set(indexPadreA, hijos.get(0));
            poblacion.set(indexPadreB, hijos.get(1));
        }

        return poblacion;
    }

    protected SortedArrayList<Cromosoma> operadorCruceOX(Cromosoma padre1, Cromosoma padre2){
        SortedArrayList<Cromosoma> hijos = new SortedArrayList<Cromosoma>();

        int puntoCorteA = this.random.nextInt(this.tamProblema);
        int puntoCorteB;

        do{
            puntoCorteB = this.random.nextInt(this.tamProblema);
        }while(puntoCorteA == puntoCorteB);

        if(puntoCorteA > puntoCorteB){
            int swapAux = puntoCorteA;
            puntoCorteA = puntoCorteB;
            puntoCorteB = swapAux;
        }

        hijos.add(getHjoOX(padre1, padre2, puntoCorteA, puntoCorteB));
        hijos.add(getHjoOX(padre2, padre1, puntoCorteA, puntoCorteB));

        return hijos;
    }

    private Cromosoma getHjoOX(Cromosoma padre1, Cromosoma padre2, int puntoCorteA, int puntoCorteB){

        int[] hijo = new int[this.tamProblema];
        int[] permutacionPadre1 = padre1.getPermutacion();
        int[] permutacionPadre2 = padre2.getPermutacion();

        System.arraycopy(permutacionPadre1, puntoCorteA, hijo, puntoCorteA, puntoCorteB - puntoCorteA + 1);

        int iteradorPadre2 = 0;

        for(int i = 0; i < hijo.length; i++){
            if((i < puntoCorteA || i > puntoCorteB)){
                hijo[i] = -1;
            }
        }

        for(int i = 0; i < hijo.length; i++){
            if((i < puntoCorteA || i > puntoCorteB)){
                while(contains(hijo, permutacionPadre2[iteradorPadre2])){
                    iteradorPadre2++;
                }

                hijo[i] = permutacionPadre2[iteradorPadre2];
                iteradorPadre2++;
            }
        }

        return new Cromosoma(hijo, funcionObjetivo(hijo));
    }

    private boolean contains(int[] array, int value){
        for (int item : array) {
            if (item == value)
                return true;
        }

        return false;
    }
}
