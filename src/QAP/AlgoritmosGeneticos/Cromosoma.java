package QAP.AlgoritmosGeneticos;

import java.util.Arrays;

public class Cromosoma implements Comparable<Cromosoma>{
    private int coste;
    private int[] permutacion;

    public Cromosoma(int[] permutacion, int coste) {
        this.permutacion = permutacion;
        this.coste = coste;
    }

    public int getCoste() {
        return coste;
    }

    public int[] getPermutacion() {
        return permutacion;
    }

    @Override
    public int compareTo(Cromosoma cromosoma) {
        int diff = this.coste - cromosoma.coste;
        if(diff != 0)
            return diff;
        else if(Arrays.equals(this.permutacion, cromosoma.permutacion))
            return 0;
        else
            return 1;

    }

    @Override
    public String toString() {
        String string = "";
        for(int alelo : permutacion){
            string += alelo + "\t";
        }

        return string;
    }
}
