package QAP.AlgoritmosMemeticos;

import QAP.AlgoritmosGeneticos.Cromosoma;
import Util.SortedArrayList;

import java.io.FileNotFoundException;

public class AM10_01Mejores extends AlgoritmoMemetico {
    public AM10_01Mejores(String path, long semilla, int tipoOperadorCruce) throws FileNotFoundException {
        super(path, semilla, tipoOperadorCruce);
    }

    @Override
    protected SortedArrayList<Cromosoma> hibridacionBL(SortedArrayList<Cromosoma> poblacion) {
        int size = (int) (poblacion.size() * 0.1);
        Cromosoma cromosomaBusquedLocal;
        for(int i = 0 ; i < size; i++){
            cromosomaBusquedLocal = this.busquedaLocal.getSolucion(poblacion.get(i));
            poblacion.set(i, cromosomaBusquedLocal);
        }

        return poblacion;
    }
}
