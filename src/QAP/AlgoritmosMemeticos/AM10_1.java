package QAP.AlgoritmosMemeticos;

import QAP.AlgoritmosGeneticos.Cromosoma;
import Util.SortedArrayList;

import java.io.FileNotFoundException;

public class AM10_1 extends AlgoritmoMemetico {

    public AM10_1(String path, long semilla, int tipoOperadorCruce) throws FileNotFoundException {
        super(path, semilla, tipoOperadorCruce);
    }

    @Override
    protected SortedArrayList<Cromosoma> hibridacionBL(SortedArrayList<Cromosoma> poblacion) {
        int size = poblacion.size();
        Cromosoma cromosomaBusquedLocal;
        for(int i = 0 ; i < size; i++){
            cromosomaBusquedLocal = this.busquedaLocal.getSolucion(poblacion.get(i));
            poblacion.set(i, cromosomaBusquedLocal);
        }

        return poblacion;
    }
}
