package QAP.AlgoritmosMemeticos;

import Main.Constantes;

import java.io.FileNotFoundException;

public class AlgoritmoMemeticoFactory {

    private AlgoritmoMemeticoFactory(){}

    public static AlgoritmoMemetico getInstance(int tipoHibridacion, int tipoCruce, long semilla, String path) throws FileNotFoundException {

        AlgoritmoMemetico algoritmoMemetico;

        switch (tipoHibridacion){
            case Constantes.AM10_1:
                algoritmoMemetico = new AM10_1(path, semilla, tipoCruce);
                break;
            case Constantes.AM10_01:
                algoritmoMemetico = new AM10_01(path, semilla, tipoCruce);
                break;
            default:
                algoritmoMemetico = new AM10_01Mejores(path, semilla, tipoCruce);
                break;
        }

        return algoritmoMemetico;
    }

}
