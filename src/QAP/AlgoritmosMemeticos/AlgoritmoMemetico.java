package QAP.AlgoritmosMemeticos;

import QAP.AlgoritmosGeneticos.AGGeneracional;
import QAP.AlgoritmosGeneticos.Cromosoma;
import QAP.AlgoritmosGeneticos.Generacion;
import QAP.ModelSolucion;
import QAP.SolucionQAP;
import Util.SortedArrayList;

import java.io.FileNotFoundException;

public abstract class AlgoritmoMemetico extends SolucionQAP {

    private static int GENERACIONES = 10;
    protected AGGeneracional algoritmoGenetico;
    protected BusquedaLocal busquedaLocal;

    private int usosFuncionObjetivo;

    public AlgoritmoMemetico(String path, long semilla, int tipoOperadorCruce) throws FileNotFoundException {
        super(path, semilla);
        this.algoritmoGenetico = new AGGeneracional(tamProblema, flujos, distancias, random, tipoOperadorCruce);
        this.busquedaLocal = new BusquedaLocal(tamProblema, flujos, distancias, random);
    }

    public ModelSolucion getSolucion(){

        long timeStart = System.currentTimeMillis();

        int generaciones = 0;
        usosFuncionObjetivo = 0;
        SortedArrayList<Cromosoma> poblacion = algoritmoGenetico.generaPoblacionInicial();

        while(usosFuncionObjetivo < 20000){
            if (generaciones == AlgoritmoMemetico.GENERACIONES){
                poblacion = hibridacionBL(poblacion);
                generaciones = 0;
            }

            Generacion siguienteGeneracion = algoritmoGenetico.siguienteGeneracion(poblacion);
            usosFuncionObjetivo += siguienteGeneracion.getUsosFuncionObjetivo();
            poblacion = siguienteGeneracion.getPoblacion();
            generaciones++;
        }

        return new ModelSolucion(System.currentTimeMillis() - timeStart, poblacion.get(0));
    }

    protected abstract SortedArrayList<Cromosoma> hibridacionBL(SortedArrayList<Cromosoma> poblacion);
}
