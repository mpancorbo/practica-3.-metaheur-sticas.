package QAP.AlgoritmosMemeticos;

import QAP.AlgoritmosGeneticos.Cromosoma;
import QAP.QAP;

import java.util.Random;

public class BusquedaLocal extends QAP {

    private static int MAXIMO_VECINOS_EXPLORADOS = 400;

    public BusquedaLocal(int tam, int[][] flujos, int[][] distancias, Random random){
        super(tam, flujos, distancias, random);
    }

    public Cromosoma getSolucion(Cromosoma cromosoma){

        int dontLookBits[] = new int[this.tamProblema];
        int[] permutacion = cromosoma.getPermutacion().clone();
        int coste = cromosoma.getCoste();

        boolean mejora;
        int vecinosExplorados = 0;

        do {
            mejora = false;
            for (int i = 0; i < this.tamProblema; i++) {
                if (dontLookBits[i] == 0 && !mejora && vecinosExplorados < BusquedaLocal.MAXIMO_VECINOS_EXPLORADOS) {
                    for (int j = 0; j < this.tamProblema; j++) {
                        if (i != j && !mejora && vecinosExplorados < BusquedaLocal.MAXIMO_VECINOS_EXPLORADOS) {
                            vecinosExplorados++;
                            int diff = getDiffCoste(permutacion, i, j);

                            if (diff < 0) {
                                mejora = true;
                                coste += diff;

                                int aux = permutacion[i];
                                permutacion[i] = permutacion[j];
                                permutacion[j] = aux;

                                dontLookBits[i] = 0;
                                dontLookBits[j] = 0;
                            }
                        }
                    }
                    if (!mejora) dontLookBits[i] = 1;
                }
            }
        }while(mejora);

        return new Cromosoma(permutacion, coste);
    }

}
