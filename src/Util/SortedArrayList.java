package Util;

import java.util.ArrayList;
import java.util.Collections;

public class SortedArrayList<T extends Comparable<T>> extends ArrayList<T> {
    @Override
    public boolean add(T item) {
        int index = Collections.binarySearch(this, item);

        //Item not found, apply unary bitwise complement to get the right index ( - (index) - 1)
        if (index < 0) index = ~index;
        super.add(index, item);

        return true;
    }

    @Override
    public T set(int i, T t) {
        T previousElement = get(i);

        remove(i);
        add(t);

        return previousElement;
    }

    @Override
    public void add(int i, T t) {
        add(t);
    }
}
