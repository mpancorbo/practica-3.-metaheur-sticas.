package Util;

import QAP.ModelSolucion;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public interface Print {

    public void printResultados(ArrayList<ModelSolucion> soluciones, int indexProblem);

    public int menuTipoSolucion();

    public int menuTipoCruce();

    public int menuProblemas();

    public int menuTipoHibridacion();

    public int menuIteraciones();

    public long menuSemilla();

    public void mostrarFileNotFoundError(FileNotFoundException exc);

    public void printCabecera();
}
