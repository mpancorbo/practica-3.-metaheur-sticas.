package Util;

import QAP.ModelSolucion;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static Main.Constantes.*;

public class PrintToConsole implements Print {

    public void printResultados(ArrayList<ModelSolucion> soluciones, int indexProblem) {
        if(soluciones.size() == 1) {
            System.out.println("Solución para el problema " + PROBLEMAS[indexProblem]);
            System.out.println("---------------------------------------");
            System.out.println(soluciones.get(0));
        }else {
            for (int i = 0; i < soluciones.size(); i++){
                System.out.println("Iteración " + (i+1) + " " + PROBLEMAS[indexProblem]);
                System.out.println(soluciones.get(i));
            }

            printEstadisticas(soluciones, indexProblem);
        }
    }

    public int menuTipoSolucion(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el método a usar para resolver el problema:");
            System.out.println("\t" + MEMETICO + "- Algoritmo Memético");
            System.out.println("\t"+ GREEDY +"- Algoritmo Greedy");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 2);

        return tipo;
    }

    private void printEstadisticas(ArrayList<ModelSolucion> soluciones, int indexProblem){
        ModelSolucion mejor = soluciones.get(0);
        ModelSolucion peor = soluciones.get(0);

        for (ModelSolucion solucion : soluciones) {
            if (solucion.getCoste() > peor.getCoste())
                peor = solucion;
            else if (solucion.getCoste() < mejor.getCoste())
                mejor = solucion;
        }

        System.out.println("Estadísticas del problema " + PROBLEMAS[indexProblem]);
        System.out.println("----------------------------------------------");

        System.out.println("Coste de la mejor solución: " + mejor.getCoste());
        System.out.println("Coste de la peor solución: " + peor.getCoste());

        double desviacion = Estadisticas.desviacionMedia(soluciones, mejor);
        double tiempoMedio = Estadisticas.tiempoMedio(soluciones);
        System.out.println("Desviación media: " + desviacion);
        System.out.println("Tiempo medio: " + tiempoMedio);
        System.out.println();
    }


    public int menuTipoCruce(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el cruce a usar para resolver el problema:");
            System.out.println("\t" + CrucePosicion + "- Cruce posición");
            System.out.println("\t"+ CrucePMX +"- Cruce PMX");
            System.out.println("\t"+ CruceOX +"- Cruce OX");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 3);

        return tipo;
    }

    public int menuProblemas(){
        Scanner in = new Scanner(System.in);
        int problemIndex;
        do{
            System.out.println("Escoge el problema que quieres resolver:");
            for(int i = 0; i < PROBLEMAS.length; i++){
                System.out.println("\t" + (i+1) + "- " + PROBLEMAS[i]);
            }
            System.out.println("\t" + (PROBLEMAS.length+1) + "- " + "Todos los problemas");
            problemIndex = in.nextInt();

        }while(problemIndex > (PROBLEMAS.length + 1) || problemIndex < 1);

        return problemIndex;
    }

    @Override
    public int menuTipoHibridacion() {
        Scanner in = new Scanner(System.in);
        int hibridacionIndex;
        do{
            System.out.println("Escoge el método de hibridación a emplear por el algoritmo memético:");
            System.out.println("\t" + AM10_1 + "- Búsqueda local sobre toda la población cada 10 generaciones");
            System.out.println("\t"+ AM10_01 + "- Búsqueda local sobre toda la población con probabilidad 0.1 cada 10 generaciones");
            System.out.println("\t"+ AM10_01Mejores + "- Búsqueda local sobre el subconjunto de los 10% mejores elementos de la población cada 10 generaciones");

            hibridacionIndex = in.nextInt();

        }while(hibridacionIndex < 1 || hibridacionIndex > 3);

        return hibridacionIndex;
    }

    public int menuIteraciones(){
        Scanner in = new Scanner(System.in);
        int numIteration;
        System.out.println("Escoge el numero de iteraciones a realizar:");
        numIteration = in.nextInt();
        return numIteration;
    }

    public long menuSemilla(){
        Scanner in = new Scanner(System.in);
        long semilla;
        System.out.println("Escoge la semilla para la solución inicial:");
        semilla = in.nextLong();

        return semilla;
    }

    @Override
    public void mostrarFileNotFoundError(FileNotFoundException exc) {
        System.out.println("La ruta especificada para el archivo no existe: " + exc.getMessage());
    }

    @Override
    public void printCabecera() {
        System.out.println("Práctica 3. Metaheurísticas");
        System.out.println();
    }
}
