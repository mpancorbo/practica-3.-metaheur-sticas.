package Util;

import QAP.ModelSolucion;

import java.util.ArrayList;

public class Estadisticas {

    public static double desviacionMedia(ArrayList<ModelSolucion> soluciones, ModelSolucion mejor){
        double desviacion = 0;
        float factor = 100 / (float)mejor.getCoste();

        for (ModelSolucion solucion : soluciones) {
            desviacion += factor * (solucion.getCoste() - mejor.getCoste());
        }
        desviacion /= soluciones.size();

        return desviacion;
    }

    public static double tiempoMedio(ArrayList<ModelSolucion> soluciones){
        double tiempoMedio = 0;

        for (ModelSolucion solucion : soluciones) {
            tiempoMedio += solucion.getTimeElapsed();
        }

        tiempoMedio /= soluciones.size();

        return tiempoMedio;
    }
}
